import './App.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Login from "../src/page/Login"
import Register from './page/Register';
import EmailConfirmSuccess from './page/Email-Confirm-Success';
import ResetPasswordEmail from './page/Reset-Password-Email';
import ResetPasswordNewPass from './page/Reset-Password-New-Pass';
import LandingPage from './page/LandingPage';
import { ListMenuKelas } from './page/ListMenuKelas';
import { DetailKelas } from './page/DetailKelas';
import { SuccessPurchase } from './page/Success-Purchase';
import { Checkout } from './page/Checkout';
import { DetailsInvoice } from './page/Details-Invoice';
import { Invoice } from './page/Invoice';
import { Myclassjadwal } from './page/My-Class-(Jadwal)';
import LandingPageUsers from './page/LandingPageuser';
import AdminUser from './page/Admin/AdminUser';
import AdminPaymentMethod from './page/Admin/AdminPaymentMethod';
import Coba from './page/coba/coba';
import { AdminInvoice } from './page/Admin/AdminInvoice';
import { DetailsInvoiceAdmin } from './page/Admin/Details-Invoice-admin';
import { ListMenuKelasLogin } from './page/BelumLogin/ListMenuKelas';
import { DetailKelasLogin } from './page/BelumLogin/DetailKelas';

function App() {
  return (
    <Router>
      <Routes>
        <Route path='/login' element={<Login/>}/>
        <Route path='/register' element={<Register/>}/>
        <Route path='/emailconfirmsuccess' element={<EmailConfirmSuccess/>}/>
        <Route path='/resetpasswordemail' element={<ResetPasswordEmail/>}/>
        <Route path='/newpassword/:token' element={<ResetPasswordNewPass/>}/>
        <Route path='/' element={<LandingPage/>}/>
        <Route path='/Homepage' element={<LandingPageUsers/>}/>
        <Route path='/listmenukelas/:id' element={<ListMenuKelas/>}/>
        <Route path='/detailkelas/:id' element={<DetailKelas/>}/>
        <Route path='/successpurchase' element={<SuccessPurchase/>}/>
        <Route path='/checkout' element={<Checkout/>}/>
        <Route path='/invoice' element={<Invoice/>}/>
        <Route path='/detailinvoice' element={<DetailsInvoice/>}/>
        <Route path='/myclass' element={<Myclassjadwal/>}/>
        <Route path='/verify/:token' element={<EmailConfirmSuccess/>}/>
        <Route path='/adminuser' element={<AdminUser/>}/>
        <Route path='/adminpaymentmethod' element={<AdminPaymentMethod/>}/>
        <Route path='/admininvoice' element={<AdminInvoice/>}/>
        <Route path='/detailinvoiceadmin' element={<DetailsInvoiceAdmin/>}/>
        <Route path='/listmenukelaslogin/:id' element={<ListMenuKelasLogin/>}/>
        <Route path='/detailkelaslogin/:id' element={<DetailKelasLogin/>}/>
      </Routes>
    </Router>
  )
}

export default App;
