import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { useNavigate } from 'react-router-dom';
import { Home, Logout, Person, ShoppingCart } from '@mui/icons-material';



const drawerWidth = 240;

export const HeaderAdmin = (props) => {
    const navigate = useNavigate();
    const { window } = props;
    const [mobileOpen, setMobileOpen] = React.useState(false);
    
    const handleDrawerToggle = () => {
        setMobileOpen((prevState) => !prevState);
    };

    const handlelogout =(e)=>{
      e.preventDefault();
      localStorage.removeItem("email");
      localStorage.removeItem("role");
      localStorage.removeItem("id_users");
      localStorage.removeItem("Token");
      navigate("/");
    }
    
    const drawer = (
        <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
        <Typography variant="h6" sx={{ my: 2 }}>
            Welcome, Admin
        </Typography>
        <Divider />
        <List>
            <ListItemButton onClick={(() => navigate('/adminuser'))} sx={{ textAlign: 'center',color: '#790b0a' }}>
                <ListItemText primary={"User"} />
            </ListItemButton>
            <ListItemButton onClick={(() => navigate('/adminpaymentmethod'))} sx={{ textAlign: 'center',color: '#790b0a' }}>
                <ListItemText primary={"Payment Method"} />
            </ListItemButton>
            <ListItemButton onClick={(() => navigate('/admininvoice'))} sx={{ textAlign: 'center',color: '#790b0a' }}>
                <ListItemText primary={"Invoice"} />
            </ListItemButton>
            <ListItemButton onClick={(()=>navigate('/homepage'))} sx={{ textAlign: 'center',color: '#790b0a' }}>
                <Home/> <ListItemText primary={"Homepage"} />
            </ListItemButton>
            <ListItemButton onClick={handlelogout} sx={{ textAlign: 'center',color: '#790b0a' }} >
                <Logout/> <ListItemText primary={"Log Out"} />
            </ListItemButton>
        </List>
        </Box>
    );
    
        const container = window !== undefined ? () => window().document.body : undefined;
  return (
    <Box sx={{ flexGrow: 1}}>
    <CssBaseline />
    <AppBar position="static" component="nav" sx={{backgroundColor:'white',boxShadow:'none'}}>
      <Toolbar sx={{marginLeft:'30px', marginRight:'30px'}}>
        <IconButton
          color="black"
          aria-label="open drawer"
          edge="start"
          onClick={handleDrawerToggle}
          sx={{ mr: 2, display: { sm: 'none' } }}
        >
          <MenuIcon />
        </IconButton>
        <img src={require('../img/Untitled.png')} style={{width:'80px', height:'Auto'}} alt='img'/>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1, color:'black' }}>
          Welcome, Admin
          </Typography>
        <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
          <Button onClick={(() => navigate('/adminuser'))} sx={{color:'#790b0a',marginRight:'20px'}}>User</Button>
          <Button onClick={(() => navigate('/adminpaymentmethod'))} sx={{color:'#790b0a',marginRight:'20px'}}>Payment Method</Button>
          <Button onClick={(() => navigate('/admininvoice'))} sx={{color:'#790b0a',marginRight:'20px'}}>Invoice</Button>
          <IconButton aria-label='user' sx={{color:'#790b0a',marginRight:'20px'}} onClick={(()=>navigate('/homepage'))}><Home/></IconButton>
          <IconButton aria-label='user' sx={{color:'#790b0a',marginRight:'20px'}} onClick={handlelogout}><Logout/></IconButton>
        </Box>
      </Toolbar>
    </AppBar>
    <Box component="nav" >
      <Drawer
        container={container}
        variant="temporary"
        open={mobileOpen}
        onClose={handleDrawerToggle}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}
        sx={{
          display: { xs: 'block', sm: 'none' },
          '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
        }}
      >
        {drawer}
      </Drawer>
    </Box>
  </Box>
  )
}
