import React, { useEffect, useState } from 'react'
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { useNavigate } from 'react-router-dom';

export const Headerawal = () => {
  const navigate = useNavigate()
  const [WindowSize, setwindowsize] = useState(window.innerWidth);
  const [judul, setjudul] = useState("");
 
  useEffect(() => {
    const handleresize = () => {
      setwindowsize(window.innerWidth);
    };
    window.addEventListener('resize', handleresize);
    if (WindowSize < 900) {
      setjudul("");

    } else {
      setjudul("Otomobil");
    }

  }, [WindowSize])
  return (
    <div>
        <Box sx={{ flexGrow: 1}} >
            <AppBar position="static" sx={{backgroundColor:'white',boxShadow: 'none'}}>
        <Toolbar sx={{marginLeft:'30px', marginRight:'30px'}}>
            <img src={require('../img/Untitled.png')} onClick={(()=>navigate('/'))} style={{width:'80px', height:'Auto'}} alt='img'/>
          <Typography onClick={(()=>navigate('/'))} variant="h6" component="div" sx={{ flexGrow: 1, color:'black' }}>
            {judul}
          </Typography>
          <Button onClick={(() => navigate('/Register'))} sx={{color:'#790b0a'}}>sign up</Button>
          <Button onClick={(() => navigate('/login'))} variant='contained' sx={{backgroundColor:'#790b0a',marginLeft:'10px',borderRadius:'10px'}}>Login</Button>
        </Toolbar>
      </AppBar>
    </Box>
    </div>
  )
}
