export const validName = new RegExp(
  '^[a-zA-Z]'  
);
export const validPassword = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*?[0-9]).{6,}$');