import React from 'react'
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { useNavigate } from 'react-router-dom';
import { Logout, Person, ShoppingCart } from '@mui/icons-material';
import { useState } from 'react';
import { useEffect } from 'react';

const drawerWidth = 240;

export const HeaderResponsive = (props) => {
    const navigate = useNavigate();
    const { window } = props;
    const [admin, setAdmin] = useState();
    const [admindrawer, setAdmindrawer] = useState();
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const role = localStorage.getItem('role');
    useEffect(()=>{
      if(role === "admin"){
        setAdmin(<IconButton aria-label='user' sx={{color:'#790b0a',marginRight:'20px'}} onClick={(()=>navigate('/adminuser'))}><Person/></IconButton>);
        setAdmindrawer(
        <ListItemButton onClick={(()=>navigate('/adminuser'))} sx={{ textAlign: 'center',color: '#790b0a'}} >
          <Person/> <ListItemText primary={"Dashboard"} />
        </ListItemButton>
        );
      }
    },[role]);
    
    // else{
    //   setAdmin("");
    // }
    
    const handleDrawerToggle = () => {
        setMobileOpen((prevState) => !prevState);
    };

    const handlelogout =(e)=>{
      e.preventDefault();
      localStorage.removeItem("email");
      localStorage.removeItem("role");
      localStorage.removeItem("Token");
      localStorage.removeItem("id_users");
      navigate("/");
    }
    
    const drawer = (
        <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
        <Typography variant="h6" sx={{ my: 2 }}>
            Otomobil
        </Typography>
        <Divider />
        <List>
            <ListItemButton onClick={(() => navigate('/checkout'))} sx={{ textAlign: 'center',color: '#790b0a'}}>
                <ShoppingCart/> <ListItemText primary={"Cart"} />
            </ListItemButton>
            <ListItemButton onClick={(() => navigate('/myclass'))} sx={{ textAlign: 'center',color: '#790b0a' }}>
                <ListItemText primary={"My Class"} />
            </ListItemButton>
            <ListItemButton onClick={(() => navigate('/invoice'))} sx={{ textAlign: 'center',color: '#790b0a' }}>
                <ListItemText primary={"Invoice"} />
            </ListItemButton>
            {admindrawer}
            <ListItemButton sx={{ textAlign: 'center',color: '#790b0a' }} onClick={handlelogout}>
                <Logout/> <ListItemText primary={"Log Out"} />
            </ListItemButton>
            {/* <ListItem key={} disablePadding>
                <ListItemButton sx={{ textAlign: 'center' }}>
                <ListItemText primary={} />
                </ListItemButton>
            </ListItem> */}
            
        </List>
        </Box>
    );
    
        const container = window !== undefined ? () => window().document.body : undefined;
    
  return (
    <Box sx={{ flexGrow: 1}}>
    <CssBaseline />
    <AppBar position="static" component="nav" sx={{backgroundColor:'white',boxShadow:'none'}}>
      <Toolbar sx={{marginLeft:'30px', marginRight:'30px'}}>
        <IconButton
          color="black"
          aria-label="open drawer"
          edge="start"
          onClick={handleDrawerToggle}
          sx={{ mr: 2, display: { sm: 'none' } }}
        >
          <MenuIcon />
        </IconButton>
        <img src={require('../img/Untitled.png')} style={{width:'80px', height:'Auto'}} alt='img'/>
          <Typography onClick={()=>navigate('/homepage')} variant="h6" component="div" sx={{ flexGrow: 1, color:'black' }}>
            Otomobil
          </Typography>
        <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
        <IconButton onClick={(() => navigate('/checkout'))} sx={{color: '#790b0a',marginRight:'20px'}}><ShoppingCart/></IconButton>
          <Button onClick={(() => navigate('/myclass'))} sx={{color:'#790b0a',marginRight:'20px'}}>My Class</Button>
          <Button onClick={(() => navigate('/invoice'))} sx={{color:'#790b0a',marginRight:'20px'}}>Invoice</Button>
          {admin}
          <IconButton aria-label='user' sx={{color:'#790b0a',marginRight:'20px'}} onClick={handlelogout}><Logout/></IconButton>
        </Box>
      </Toolbar>
    </AppBar>
    <Box component="nav" >
      <Drawer
        container={container}
        variant="temporary"
        open={mobileOpen}
        onClose={handleDrawerToggle}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}
        sx={{
          display: { xs: 'block', sm: 'none' },
          '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
        }}
      >
        {drawer}
      </Drawer>
    </Box>
  </Box>
);
  
}

