import React from 'react'
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import { useNavigate } from 'react-router-dom';
import { Logout, Person, ShoppingCart } from '@mui/icons-material';
export const Headeruser = () => {
const navigate = useNavigate();
  return (
    <div>
        <Box sx={{ flexGrow: 1}} >
            <AppBar position="static" sx={{backgroundColor:'white',boxShadow: 'none'}}>
        <Toolbar sx={{marginLeft:'30px', marginRight:'30px'}}>
            <img src={require('../img/Untitled.png')} style={{width:'80px', height:'Auto'}} alt='img'/>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1, color:'black' }}>
            Otomobil
          </Typography>
          <IconButton sx={{color: '#790b0a',marginRight:'20px'}}><ShoppingCart/></IconButton>
          <Button onClick={(() => navigate(''))} sx={{color:'#790b0a',marginRight:'20px'}}>My Class</Button>
          <Button onClick={(() => navigate(''))} sx={{color:'#790b0a',marginRight:'20px'}}>Invoice</Button>
          <IconButton aria-label='user' sx={{color:'#790b0a',marginRight:'20px'}}><Person/></IconButton>
          <IconButton aria-label='user' sx={{color:'#790b0a',marginRight:'20px'}}><Logout/></IconButton>
        </Toolbar>
      </AppBar>
    </Box>
    </div>
  )
}
