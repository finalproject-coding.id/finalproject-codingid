import React from 'react'
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

export const HeaderConfirm = () => {
  return (
    <div>
         <Box sx={{ flexGrow: 1}}>
            <AppBar position="static" sx={{backgroundColor:'white',boxShadow: 'none'}}>
        <Toolbar sx={{marginLeft:'30px', marginRight:'30px'}}>
            <img src={require('../img/Untitled.png')} style={{width:'80px', height:'Auto'}} alt='img'/>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1, color:'black' }}>
            Otomobil
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
    </div>
  )
}
