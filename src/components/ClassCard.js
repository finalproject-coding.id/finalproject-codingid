import { Card, CardActionArea, CardContent, CardMedia } from '@mui/material'
import React from 'react'
import { Link } from 'react-router-dom'


const ClassCard = ({idtype,idclass,namatype,namaclass,hargaclass,imgclass})=>{
  return(
  <Card sx={{ maxWidth:'345px', height:'450px', marginBottom:'20px', textDecoration:"none"}} >
      <CardActionArea component={Link}
      to={`/detailkelas/${idtype}`}
      state={{
        id:idclass,
        type:namatype,
        nama:namaclass,
        harga:hargaclass,
        img:imgclass,
      }}>
      <CardMedia
        component="img"
        alt="green iguana"
        height="auto"
        image={imgclass}
      />
      <CardContent>
        <h3 style={{ color:'gray'}}>{namatype}</h3>
        <h3>{namaclass}</h3>
        <h2 style={{color:'#790b0a',marginTop:'40px'}}>
          IDR {new Intl.NumberFormat('id-ID', {currency: 'IDR' }).format(hargaclass)}
        </h2> 
      </CardContent>
      </CardActionArea>
    </Card> 
  )
}

export default ClassCard;