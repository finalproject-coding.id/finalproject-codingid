import { Call, Email, Instagram, Telegram, YouTube } from '@mui/icons-material'
import { Box, Grid, IconButton } from '@mui/material'
import React from 'react'

export const Footer = () => {
  return (
    <div>
       <hr></hr>
        <Grid container style={{marginBottom : '20px'}}>
            <Grid item lg={1} sm={1} md={1} xs={0}></Grid>
            <Grid item lg={4} sm={4} md={4} xs={12}  sx={{}}>
                <h3 style={{color:'#790b0a'}}>About Us</h3>
                <span style={{textAlign:'justify'}}>Sed ut perspiciatis unde omnis iste natus error sit <br/>
                volupatatern accusantium doloremque <br/> 
                laudantium, totam rem aperiam, eaqque ipsa <br/>
                quae ab illo inventore veritatis et quasi architecto <br/>
                beatae vitae dicta sunt explicabo <br/><br/><br/></span>
            </Grid>
            <Grid item lg={3} sm={3} md={3} xs={12} >
                <h3 style={{color:'#790b0a'}}>Product</h3>
                <Grid container>
                    <Grid item xs={4}>
                    <ul style={{paddingLeft:'20px'}}>
                            <li style={{marginBottom:'10px'}}>Electric</li>
                            <li style={{marginBottom:'10px'}}>LCGC</li>
                            <li style={{marginBottom:'10px'}}>Offroad</li>
                            <li style={{marginBottom:'10px'}}>SUV</li>
                        </ul>
                    </Grid>
                    <Grid item xs={8}>
                    <ul style={{paddingLeft:'20px'}}>
                            <li style={{marginBottom:'10px'}}>Hatchback</li>
                            <li style={{marginBottom:'10px'}}>MPV</li>
                            <li style={{marginBottom:'10px'}}>Sedan</li>
                            <li style={{marginBottom:'10px'}}>Truck</li>
                        </ul>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item lg={4} sm={4} md={4} xs={12} >
                <h3 style={{color:'#790b0a'}}>Address</h3>
                <span>sed ut perspiciatis unde omnis iste natus error sit <br/>
                volupatatern accusantium doloremque</span>
                <h3 style={{color:'#790b0a'}}>Contact Us</h3>
                <Box>
                    <IconButton sx={{backgroundColor:'#790b0a', color:'white', marginRight:'10px'}} aria-label='phone'><Call/></IconButton>
                    <IconButton sx={{backgroundColor:'#790b0a', color:'white', marginRight:'10px'}} aria-label='phone'><Instagram/></IconButton>
                    <IconButton sx={{backgroundColor:'#790b0a', color:'white', marginRight:'10px'}} aria-label='phone'><YouTube/></IconButton>
                    <IconButton sx={{backgroundColor:'#790b0a', color:'white', marginRight:'10px'}} aria-label='phone'><Telegram/></IconButton>
                    <IconButton sx={{backgroundColor:'#790b0a', color:'white'}} aria-label='phone'><Email/></IconButton>
                </Box>
            </Grid>
        </Grid>
    </div>
  )
}
