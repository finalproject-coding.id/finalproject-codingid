import React, { useEffect, useState } from 'react'
import { Box, Breadcrumbs, Grid, Link, Paper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@mui/material'
import { useLocation, useNavigate } from 'react-router-dom'
import axios from 'axios'
import { HeaderAdmin } from '../../components/HeaderAdmin'

export const DetailsInvoiceAdmin = () => {
    const navigate = useNavigate();
    const [DetailInvoice, setDetailinvoice] = useState([]);
    const BaseAPI = process.env.REACT_APP_API_URL;
    let data = useLocation();
    let id_invoice = "";
    let date = "";
    let subtotal = 0;
    if(data.state != null){
        id_invoice = data.state.id_invoice;
        date = data.state.date;
        subtotal = data.state.subtotal;
    }else{
        navigate('/admininvoice');
    }

    const GetDetailinvoice = () =>{
        var config = {
        method:"get",
        url:BaseAPI+"Invoice/GetDetailInvoice?idinvoice="+id_invoice,
        headers:{}
        };
        axios(config).then((response)=>{
            console.log(response.data);
            setDetailinvoice(response.data);
        })
    }

    useEffect(()=>{
        if(localStorage.getItem("role")==null ||localStorage.getItem("role")!="admin" ){
            navigate('/login');
        }
        if(data.state == null){
            navigate('/admininvoice');
        }
        GetDetailinvoice();
    },[])
    
    
  return (
    <div>
        <HeaderAdmin/>
         <Grid container sx={{minHeight:'60vh'}}>
            <Grid item xs={1}></Grid>
            <Grid item xs={10}>
                <Grid container sx={{mb:'2rem'}}>
                    <Grid item xs={6}>
                    <Stack spacing={2}>
                    <Breadcrumbs separator=">" aria-label="breadcrumb">
                        <Link underline="hover" color="Gray" href='/homepage'>
                        Home
                        </Link>
                        <Link underline="hover" color='gray' href='/admininvoice'>
                        Invoice
                        </Link>
                        <Link underline="hover" color="#790b0a" >
                        Details Invoice
                        </Link>
                    </Breadcrumbs>
                    </Stack>
                    <h4>Details Invoice</h4>
                    <table>
                        <tr>
                            <td>No Invoice:</td>
                            <td style={{textAlign:'end'}}>{id_invoice}</td>
                        </tr>
                        <tr>
                            <td>Date:</td>
                            <td style={{textAlign:'end'}}>{date}</td>
                        </tr>
                    </table>
                    </Grid>
                    <Grid item xs={6} >
                        <Box sx={{paddingTop:'7rem'}}><h4 style={{textAlign:'end'}}>Total Price IDR {new Intl.NumberFormat('id-ID', {currency: 'IDR' }).format(subtotal)}</h4></Box>
                    </Grid>
                </Grid>
                <Paper sx={{width:'100%'}}>
                <TableContainer >
                <Table width='100%'>
                    <TableHead sx={{backgroundColor:'#790b0a'}}>
                        <TableRow >
                            <TableCell width='10px'  sx={{color:'white'}}>No</TableCell>
                            <TableCell sx={{color:'white',textAlign:'center'}}>Course Name</TableCell>
                            <TableCell width='30px' sx={{color:'white',textAlign:'center'}}>Type</TableCell>
                            <TableCell width='40px' sx={{color:'white',textAlign:'center'}}>Schedule</TableCell>
                            <TableCell width='180px' sx={{color:'white',textAlign:'center'}}>Price</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {DetailInvoice.map((item,key)=>(
                            <TableRow key={key}>
                            <TableCell width='10px' textAlign='center'>{key+1}</TableCell>
                            <TableCell sx={{textAlign:'center'}}>{item.nama_kelas}</TableCell>
                            <TableCell width='30px' sx={{textAlign:'center'}}>{item.namatipe}</TableCell>
                            <TableCell width='40px' sx={{textAlign:'center'}}>{item.tanggal}</TableCell>
                            <TableCell width='180px' sx={{textAlign:'center'}}>IDR {new Intl.NumberFormat('id-ID', {currency: 'IDR' }).format(item.hargakelas)}</TableCell>
                        </TableRow>
                        ))}
                        
                        
                    </TableBody>
                </Table>
                </TableContainer>
                </Paper>
            </Grid>
            <Grid item xs={1}></Grid>
         </Grid>
    </div>
  )
}
