import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import { styled } from '@mui/material/styles';
import { Edit, Delete } from '@material-ui/icons';
import { HeaderConfirm } from '../../components/HeaderConfirm';
import { Link, NavLink, useNavigate } from 'react-router-dom';
import { HeaderAdmin } from '../../components/HeaderAdmin';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, FormControl, FormControlLabel, Grid, IconButton, InputLabel, MenuItem, Paper, Select, Switch, Table, TableBody, TableContainer, TableHead, TableRow, TextField } from '@mui/material';
import { Add } from '@mui/icons-material';
import EditIcon from '@mui/icons-material/Edit';
import { hover } from '@testing-library/user-event/dist/hover';
import axios, { all } from 'axios';

const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 650,
    },
    dialog: {
        [theme.breakpoints.up('sm')]: {
            minWidth: 500,
        },
    },
}));
const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: "#790b0a",
      color: theme.palette.common.white,
      textAlign:"center"
      
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));
  
  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(even)': {
      backgroundColor: '#f2e7e6',
      
      
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

export const AdminInvoice = () => {
    const navigate = useNavigate();
    const classes = useStyles();
    const token = localStorage.getItem('Token');
    const [Invoice,setInvoice] = useState([]);
    const BaseAPI = process.env.REACT_APP_API_URL;
    const GetInvoice = () =>{
        var config = {
          method:"get",
          url:BaseAPI+"Invoice/GetInvoicebyAdmin",
          headers:{'Authorization': `bearer ${token}`}
          };
          axios(config).then((response)=>{
              console.log(response.data);
              setInvoice(response.data);
          })
      }
    
      useEffect(()=>{
        GetInvoice();
        if(localStorage.getItem("role")==null ||localStorage.getItem("role")!="admin"){
          navigate('/login');
        }
      },[])
    
  return (
    <div>
        <HeaderAdmin/>
        <Grid container spacing={3} >
        <Grid item xl={1} lg={1} md={1} xs={0}>
            
        </Grid>
        <Grid item xl={10} lg={10} md={10} xs={12}>
            <h1 style={{textAlign:'center'}}>Invoice Otomobil</h1>
            
            <Paper sx={{width:'100%', pt:'2rem'}}>
                <TableContainer>
                <Table>
                <TableHead>
                    <TableRow>
                        <StyledTableCell>No</StyledTableCell>
                        <StyledTableCell>No. Invoice</StyledTableCell>
                        <StyledTableCell>Date</StyledTableCell>
                        <StyledTableCell>Total Course</StyledTableCell>
                        <StyledTableCell>Total Price</StyledTableCell>
                        <StyledTableCell>Action</StyledTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                {Invoice.map((item,key)=>(
                <StyledTableRow key={key}>
                <TableCell sx={{textAlign:'center'}}>{key+1}</TableCell>
                <TableCell sx={{textAlign:'center'}}>{item.id_invoice}</TableCell>
                <TableCell sx={{textAlign:'center'}}>{item.tanggalinvoice}</TableCell>
                <TableCell sx={{textAlign:'center'}}>{item.totalcourse}</TableCell>
                <TableCell sx={{textAlign:'center'}}>IDR {new Intl.NumberFormat('id-ID', {currency: 'IDR' }).format(item.subtotal)}</TableCell>
                <TableCell sx={{textAlign:'center'}}>
                    <NavLink component={Link} to={`/detailinvoiceadmin`}
                    state={{
                        id_invoice:item.id_invoice,
                        date: item.tanggalinvoice,
                        subtotal: item.subtotal,
                    }}
                    >
                    <Button  type='outlined' sx={{color:'#790b0a',backgroundColor:'white', border:'2px solid', borderRadius:'10px', width:'100px'}}>Details</Button>
                    </NavLink>
                  </TableCell>
                  </StyledTableRow>
                ))}
                  
              </TableBody>
              </Table>
              </TableContainer>
          </Paper>
      </Grid>
      <Grid item xl={1} lg={1} md={1} xs={0}>
          
      </Grid>
      
  </Grid>
    </div>
  )
}
