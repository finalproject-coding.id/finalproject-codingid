import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Typography, Button, TextField, Grid, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Switch } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { useNavigate } from 'react-router-dom';
import { HeaderConfirm } from '../../components/HeaderConfirm';
import { HeaderAdmin } from '../../components/HeaderAdmin';
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
    container: {
        margin: 'auto',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    tableResponsive: {
        overflowX: 'scroll',
        [theme.breakpoints.up('sm')]: {
            overflowX: 'auto',
        },
    },
    table: {
        width: '100%',
        maxWidth: '100%',
        backgroundColor: 'transparent',
        borderCollapse: 'collapse',
    },
    tableCellLabel: {
        display: 'block',
        textAlign: 'center',
        height: '100%',
        width: '100%',
    },
    tableCellLabelBefore: {
        content: 'attr(data-label)',
        float: 'left',
        fontWeight: 'bold',
        textTransform: 'uppercase',
    },
    activeSwitch: {
        color: '#790b0a',
        '&$checked': {
            color: '#790b0a',
        },
    },
    title:{
        fontWeight:'bold',
    },
    checked: {},
}));



const paymentMethods = [];

const AdminPaymentMethod = () => {
    const classes = useStyles();
    const [isAddMode, setIsAddMode] = useState(false);
    const [isEditMode, setIsEditMode] = useState(false);
    const [selectedPaymentMethod, setSelectedPaymentMethod] = useState(null);
    const [paymentMethodList, setPaymentMethodList] = useState(paymentMethods);
    const [newPaymentMethodName, setNewPaymentMethodName] = useState('');
    const [newPaymentMethodLogo, setNewPaymentMethodLogo] = useState(null);

    const handleAddPaymentMethod = () => {
        setIsAddMode(true);
    };

    const handleEditPaymentMethod = (paymentMethod) => {
        setSelectedPaymentMethod(paymentMethod);
        setIsEditMode(true);
    };


    const apiUrl = process.env.REACT_APP_API_URL + "PaymentMethod/addpayment";
    const baseUrl = process.env.REACT_APP_API_URL;

    const handleSavePaymentMethod = () => {
        if (isAddMode) {
            const newPaymentMethod = { id: paymentMethodList.length + 1, name: newPaymentMethodName, logoUrl: newPaymentMethodLogo, active: true };
            let data = JSON.stringify(newPaymentMethod);
            console.log(data);
            let config = {
                method: "post",
                maxBodyLength: Infinity,
                url: apiUrl,
                headers: {
                    "Content-Type": "application/json",
                },
                data: data,
            };
            axios.request(config).then((response) => {
                console.log(response.data);
            })

            setNewPaymentMethodName('');
            setNewPaymentMethodLogo(null);
        } else if (isEditMode) {

            paymentMethodList.map((pm) => {
                if (pm.id === selectedPaymentMethod.id) {
                    const editpayment = { ...pm, name: selectedPaymentMethod.name, logoUrl: newPaymentMethodLogo, active: selectedPaymentMethod.active };
                    console.log(editpayment);
                    let data = JSON.stringify(editpayment);
                    console.log(data);
                    let config = {
                        method: "post",
                        maxBodyLength: Infinity,
                        url: baseUrl + "PaymentMethod/update",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        data: data,
                    };
                    axios.request(config).then((response) => {
                        console.log(response.data);
                        GetData();
                    })
                }

            }

            );

            // setSelectedPaymentMethod(null);

        }
        setIsAddMode(false);
        setIsEditMode(false);
    };



    const handleCancel = () => {
        setIsAddMode(false);
        setIsEditMode(false);
        setSelectedPaymentMethod(null);
        setNewPaymentMethodName('');
        setNewPaymentMethodLogo(null);
    };

    const handleChangeActive = (paymentMethod) => {
        let active = false;
        if (paymentMethod.active === true) {
            active = false;
        } else {
            active = true;
        }
        enableactive(paymentMethod.id, active);
    };

    const enableactive = async (id, active) => {
        let result = await axios.get(baseUrl + "PaymentMethod/enableactive?id=" + id + "&active=" + active);
        console.log(result.data);
        GetData();
    }

    const handleLogoUpload = (e) => {
        const file = e.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onloadend = () => {
                setNewPaymentMethodLogo(reader.result);
            };
        }
    };

    const GetData = () => {
        var config = {
            method: "get",
            url: baseUrl + "PaymentMethod/getpayment",
            headers: {}
        };
        axios(config).then((response) => {
            // console.log(response.data);
            setPaymentMethodList(response.data);
        })
    }

    useEffect(() => {
        GetData();
    })

    const navigate = useNavigate();

    return (
        <div>
            <HeaderAdmin />
            <Container className={classes.container} >
                <Typography variant="h5" className={classes.title}>
                    Payment Methods
                </Typography>
                <Grid container spacing={2} alignItems="center">
                    <Grid item>
                        <Button variant="contained" style={{ backgroundColor: "#790b0a", color: "white" }} startIcon={<AddIcon />} onClick={handleAddPaymentMethod}>
                            Add Payment Method
                        </Button>
                    </Grid>
                </Grid>
                <br></br>
                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="Payment Method Table">
                        <TableHead>
                            <TableRow style={{ backgroundColor: "#790b0a" }}>
                                <TableCell style={{ color: "white" }}>Name</TableCell>
                                <TableCell style={{ color: "white" }} align="center">Logo</TableCell>
                                <TableCell style={{ color: "white" }} align="center">Status</TableCell>
                                <TableCell style={{ color: "white" }} align="center">Actions</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {paymentMethodList.map((paymentMethod) => (
                                <TableRow key={paymentMethod.id}>
                                    <TableCell component="th" scope="row">
                                        {isEditMode && selectedPaymentMethod?.id === paymentMethod.id ?
                                            <TextField
                                                value={selectedPaymentMethod.name}
                                                onChange={(e) => setSelectedPaymentMethod({ ...selectedPaymentMethod, name: e.target.value })}
                                                variant="outlined"
                                                size="small"
                                                autoFocus
                                            />
                                            :
                                            paymentMethod.name
                                        }
                                    </TableCell>
                                    <TableCell align="center"  >
                                        {isEditMode && selectedPaymentMethod.id === paymentMethod.id ? (
                                            <input type="file" onChange={handleLogoUpload} accept="image/*" />
                                        ) : (
                                            <img src={paymentMethod.logoUrl} alt={paymentMethod.name} className={classes.logo} style={{ width: '50px', height: 'auto' }} />
                                        )}
                                    </TableCell>
                                    <TableCell align="center">
                                        <Switch
                                            checked={paymentMethod.active}
                                            onChange={() => handleChangeActive(paymentMethod)}
                                            name="active"
                                            inputProps={{ 'aria-label': 'Payment Method Active Switch' }}
                                            classes={{
                                                switchBase: classes.activeSwitch,
                                                checked: classes.checked,
                                            }}
                                        />
                                    </TableCell>
                                    <TableCell align="center">
                                        {isEditMode && selectedPaymentMethod?.id === paymentMethod.id ?
                                            <>
                                                <Button variant="contained" color="primary" onClick={handleSavePaymentMethod}>
                                                    Save
                                                </Button>
                                                <Button variant="contained" onClick={handleCancel}>
                                                    Cancel
                                                </Button>
                                            </>
                                            :
                                            <>
                                                <Button variant="contained" startIcon={<EditIcon />} onClick={() => handleEditPaymentMethod(paymentMethod)}>
                                                    Edit
                                                </Button>
                                            </>
                                        }
                                    </TableCell>
                                </TableRow>
                            ))}
                            {isAddMode &&
                                <TableRow>
                                    <TableCell>
                                        <TextField
                                            value={newPaymentMethodName}
                                            onChange={(e) => setNewPaymentMethodName(e.target.value)}
                                            variant="outlined"
                                            size="small"
                                            autoFocus
                                        />
                                    </TableCell>
                                    <TableCell align="center">
                                        <input type="file" onChange={handleLogoUpload} accept="image/*" />
                                        {newPaymentMethodLogo && <img src={newPaymentMethodLogo} alt="New Payment Method Logo" className={classes.logo} style={{ width: '50px', height: 'auto' }} />}
                                    </TableCell>
                                    <TableCell align="center">
                                        <Switch
                                            checked={true}
                                            name="active"
                                            inputProps={{ 'aria-label': 'New Payment Method Active Switch' }}
                                            style={{ color: '#790b0a' }}
                                        />
                                    </TableCell>
                                    <TableCell align="center">
                                        <Button variant="contained" color="primary" onClick={handleSavePaymentMethod}>
                                            Save
                                        </Button>
                                        <Button variant="contained" onClick={handleCancel}>
                                            Cancel
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            }
                        </TableBody>
                    </Table>
                </TableContainer>
            </Container>
        </div>
    );
};

export default AdminPaymentMethod;
