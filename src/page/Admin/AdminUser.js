import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import { styled } from '@mui/material/styles';
import { Edit, Delete } from '@material-ui/icons';
import { HeaderConfirm } from '../../components/HeaderConfirm';
import { useNavigate } from 'react-router-dom';
import { HeaderAdmin } from '../../components/HeaderAdmin';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, FormControl, FormControlLabel, Grid, IconButton, InputLabel, MenuItem, Paper, Select, Switch, Table, TableBody, TableContainer, TableHead, TableRow, TextField } from '@mui/material';
import { Add } from '@mui/icons-material';
import EditIcon from '@mui/icons-material/Edit';
import { hover } from '@testing-library/user-event/dist/hover';
import axios, { all } from 'axios';
import AlertModal from '../../components/AlertModal';

const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 650,
    },
    dialog: {
        [theme.breakpoints.up('sm')]: {
            minWidth: 500,
        },
    },
}));
const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: "#790b0a",
      color: theme.palette.common.white,
      textAlign:"center"
      
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));
  
  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(even)': {
      backgroundColor: '#f2e7e6',
      
      
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

const initialUser = {
    name: '',
    email: '',
    role: '',
    active: false,
};

const usersData = [
    {
        id: 1,
        name: 'Fawwaz Kumudani Widyadhana',
        email: 'fawwazkwidyadhana@gmail.com',
        role: 'Admin',
        active: true,
    },
    {
        id: 2,
        name: 'Jordy',
        email: 'jordy@gmail.com',
        role: 'Admin',
        active: true,
    },
    {
        id: 3,
        name: 'Boe Jiden',
        email: 'joebiden@gmail.com',
        role: 'User',
        active: false,
    },
];

const AdminUser = () => {
    const [showModal, setShowModal] = useState(false);
    const [modalMassage, setModalMassage] = useState("");
    const [modalTitle, setModalTitle] = useState("");
    const classes = useStyles();
    const [users, setUsers] = useState(usersData);
    const [allUser, setAllUser] = useState([]);
    const apiUrl = process.env.REACT_APP_API_URL +"User/GetUser?name=";
    const BaseApi = process.env.REACT_APP_API_URL;
    const [isEdit, setIsEdit] = useState(false);
    const [currentUser, setCurrentUser] = useState(
        {
            pk_id_user:"",
            email:"",
            nama:"",
            role:"",
        }
    );
    const [open, setOpen] = useState(false);
    const token = localStorage.getItem('Token');
    const [data,setData] = useState({
        email:"",
        name:"",
        role:"",
      });
    
    const GetUser = ()=>{
        var config = {
            method:"get",
            url:apiUrl,
            headers:{'Authorization': `bearer ${token}`}
        };
        axios(config).then((response)=>{
            // console.log(response.data);
            setAllUser(response.data);
        })
    }

    const handleSwitchChange = (e) => {
       
        let id = e.target.name;
        let status = 0;
        allUser.map((user,key)=>{
            if(user.pk_id_user == id){
                if(user.status == true){
                    status = 0;
                }else{
                    status = 1;
                }
            }
        })

        disabledUser(id,status);
       
     };

     const disabledUser = async(id,status)=>{
        try{
          let result = await axios.get(BaseApi+"User/DisabledUser?id="+id+"&status="+status);
          console.log(result.data);
          GetUser();
          
        }catch(error){
          
        }
      } 

    useEffect(()=>{
        GetUser();
        
    },[])

    const handleinput = (e) =>{
        const name = e.target.name;
        const value = e.target.value;
        setData({...data,[name]:value});
    }

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        setIsEdit(false);
        setCurrentUser(initialUser);
    };

    const handleAddUser = () => {
       //add user;
       const newuser = {
        nama: currentUser.nama,
        email: currentUser.email,
        role: currentUser.role,
       };
       let data = JSON.stringify(newuser);
       let config = {
        method: "post",
        maxBodyLength: Infinity,
        url: BaseApi+"User/AddUserAdmin",
        headers: {
          "Content-Type": "application/json",
        },
        data: data,
      };
      axios
        .request(config)
        .then((response) => {
          if(response.data === "Success"){
            GetUser();
          }
          else{
            setShowModal(true);
            setModalTitle("Error!!");
            setModalMassage( response.data+" !!");
          }
        })
        handleClose();
    };

    //Edit USer;
    const handleEditUser = (e) => {
        e.preventDefault();
        const Updateuser = {
            pk_id_user:currentUser.pk_id_user,
            nama:currentUser.nama,
            email:currentUser.email,
            role:currentUser.role
          };
        let data = JSON.stringify(Updateuser);
        let config = {
            method: "post",
            maxBodyLength: Infinity,
            url: BaseApi+"User/UpdateUser",
            headers: {
              "Content-Type": "application/json",
            },
            data: data,
          };
          axios
            .request(config)
            .then((response) => {
              console.log(JSON.stringify(response.data));
              GetUser();
            })
        handleClose();
    };

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setCurrentUser({ ...currentUser, [name]: value });
    };

    const handleEditClick = (user) => {
        setIsEdit(true);
        setCurrentUser(user);
        handleOpen();
    };

    const handleCloseModal = () => {
        setShowModal(false);
    };

    const navigate = useNavigate();
    return (
        <div>
            <HeaderAdmin/>
            <Grid container spacing={3} >
                <Grid item xl={1} lg={1} md={1} xs={0}>
                    
                </Grid>
                <Grid item xl={10} lg={10} md={10} xs={12}>
                    <h1 style={{textAlign:'center'}}>User Otomobil</h1>
                    <Button variant="contained" sx={{backgroundColor:'#790b0a' , '&:hover': { backgroundColor: '#790b0a' }}} onClick={handleOpen}><Add/> &nbsp; Add user</Button>
                    <Paper sx={{width:'100%', pt:'2rem'}}>
                        <TableContainer>
                        <Table>
                        <TableHead>
                            <TableRow>
                            <StyledTableCell>Id</StyledTableCell>
                            <StyledTableCell>Nama</StyledTableCell>
                            <StyledTableCell>Email</StyledTableCell>
                            <StyledTableCell>Role</StyledTableCell>
                            <StyledTableCell>Status</StyledTableCell>
                            <StyledTableCell>Action</StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {allUser.map((user,key)=>(
                                <StyledTableRow key={key}>
                                <TableCell sx={{textAlign:'center'}}>{user.pk_id_user}</TableCell>
                                <TableCell sx={{textAlign:'center'}}>
                                    {user.nama}
                                </TableCell>
                                <TableCell sx={{textAlign:'center'}}>
                                    {user.email}
                                </TableCell>
                                <TableCell sx={{textAlign:'center'}}>
                                    {user.role}
                                </TableCell>
                                <TableCell sx={{textAlign:'center'}}>
                                    <Switch onChange={handleSwitchChange} name={user.pk_id_user} checked={user.status} ></Switch>
                                </TableCell>
                                <TableCell sx={{textAlign:'center'}}>
                                    <IconButton onClick={()=>handleEditClick(user)}><EditIcon/></IconButton>
                                </TableCell>
                                </StyledTableRow>
                            ))}
                            
                        </TableBody>
                        </Table>
                        </TableContainer>
                    </Paper>
                </Grid>
                <Grid item xl={1} lg={1} md={1} xs={0}>
                    
                </Grid>
                
            </Grid>

            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="form-dialog-title"
                classes={{ paper: classes.dialog }}
            >
                <DialogTitle id="form-dialog-title">
                    {isEdit ? 'Edit User' : 'Add User'}
                </DialogTitle>
                <DialogContent>
                    <TextField
                        margin="dense"
                        id="nama"
                        label="Name"
                        type="text"
                        fullWidth
                        name="nama"
                        value={currentUser.nama}
                        onChange={handleInputChange}
                    />
                    <TextField
                       id="outlined-basic" 
                       name='email' 
                       fullWidth 
                       onChange={handleInputChange} 
                       type='email' label="Email" 
                       variant="outlined" 
                       value={currentUser.email}
                       />
                    <FormControl fullWidth margin="dense">
                        <InputLabel id="role-label">Role</InputLabel>
                        <Select
                            labelId="role-label"
                            id="role"
                            name="role"
                            value={currentUser.role}
                            onChange={handleInputChange}
                        >
                            <MenuItem value="users">Users</MenuItem>
                            <MenuItem value="admin">Admin</MenuItem>
                        </Select>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    {isEdit ? (
                        // 
                        <Button onClick={handleEditUser} color="primary">
                            Save
                        </Button>
                    ) : (
                        <Button onClick={handleAddUser} color="primary">
                            Add
                        </Button>
                    )}
                </DialogActions>
            </Dialog>
            <AlertModal
                open={showModal}
                onClose={handleCloseModal}
                title={modalTitle}
                message={modalMassage}
            />
        </div>
    );
};

export default AdminUser;
