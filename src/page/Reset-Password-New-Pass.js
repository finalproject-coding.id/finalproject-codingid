import React, { useEffect, useState } from 'react'
import { Headerawal } from '../components/HeaderAwal'
import { Button, Grid, TextField, Box } from '@mui/material'
import { json, useNavigate, useParams } from 'react-router-dom'
import AlertModal from '../components/AlertModal'
import axios from 'axios'

const ResetPasswordNewPass = () => {
    const navigate = useNavigate();
    let {token} = useParams();
    const [data, setData] = useState({
      password:"",
      cpassword: "",
    });
    const apiUrl = process.env.REACT_APP_API_URL +"User/newpassword";
    const [WindowSize, setwindowsize] = useState(window.innerWidth);
    const [brown, setbrown] = useState(0);
    const [pink, setpink] = useState(0);
    const [showModal, setShowModal] = useState(false);
    const [modalMassage, setModalMassage] = useState("");
    const [modalTitle, setModalTitle] = useState("");
    const handleCloseModal = () => {
      setShowModal(false);
    };
    useEffect(()=>{
      const handleresize = () => {
        setwindowsize(window.innerWidth);
      };
      window.addEventListener('resize',handleresize);
      if(WindowSize < 900){
        setbrown(8);
        setpink(2);
        console.log(WindowSize);
        
      }else{
        setbrown(6);
        setpink(3);
      }
    
    },[WindowSize])


    const inputDTO =(e)=>{
      const name = e.target.name;
      const value = e.target.value;

      setData({...data,[name]:value});
    }

    const handlenewpassword = (e)=> {
      e.preventDefault();
      const newpass = {
        token:token,
        password:data.password,
      };
      
      if(data.password === data.cpassword){
        let data = JSON.stringify(newpass);
        // console.log(data)
        let config = {
          method: "post",
          maxBodyLength: Infinity,
          url: apiUrl,
          headers: {
            "Content-Type": "application/json",
          },
          data: data,
        };
        axios
          .request(config)
          .then((response) => {
            // console.log(JSON.stringify(response.data));
            if(response.data === "Success update password"){
              setShowModal(true);
              setModalTitle("Congratulation");
              setModalMassage("Your New Password !");
            }
            else{
              setShowModal(true);
              setModalTitle("Error");
              setModalMassage(response.data);
            }
            
          })
      } else {
        setShowModal(true);
        setModalTitle("Error!");
        setModalMassage("Password do not match!");
      }
      
    }
    return (
        <div>
          <Headerawal/>
          <Grid container>
        <Grid item xs={pink}></Grid>
        <Grid item xs={brown}>
          <Box sx={{fontSize:'24px',marginTop:'80px'}}>Create Password</Box>
          <form onSubmit={handlenewpassword}>
          <Box sx={{mt:'40px'}}> <TextField sx={{width:'100%'}} id="outlined-basic" type='password' name='password' onChange={inputDTO} label="New Password" variant="outlined"/></Box>
          <Box sx={{mt:'20px'}}> <TextField sx={{width:'100%'}} id="outlined-basic" type='password' name='cpassword' onChange={inputDTO} label="Confirm New Password" variant="outlined"/></Box>
          <Box sx={{ mt: '30px', display: 'flex', justifyContent: 'flex-end' }}>
            <Button onClick={(() => navigate('/login'))} variant='contained' sx={{ backgroundColor: '#fff', color: '#790b0a', width: '150px', borderRadius: '10px', border: '2px solid #790b0a', '&:hover': { backgroundColor: '#fff' }, mr: 2 }}>Cancel</Button>
            <Button type='submit' variant='contained' sx={{ backgroundColor: '#790b0a', width: '150px', borderRadius: '10px', '&:hover': { backgroundColor: '#790b0a' }, ml: 2 }}>Confirm</Button>
          </Box>
          </form>
          
          </Grid>
        <Grid item xs={pink} ></Grid>
        </Grid>
        <AlertModal
        open={showModal}
        onClose={handleCloseModal}
        title={modalTitle}
        message={modalMassage}
      />          
        </div>
      )
}
export default ResetPasswordNewPass
