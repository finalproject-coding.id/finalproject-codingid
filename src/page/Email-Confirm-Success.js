import React, { useEffect } from 'react'
import { useNavigate, useParams} from 'react-router-dom';
import { Container } from '@mui/material';
import { HeaderConfirm } from '../components/HeaderConfirm';
import axios from 'axios';

const EmailConfirmSuccess = () => {
  const navigate = useNavigate()
  let {token} = useParams();
  const apiUrl = process.env.REACT_APP_API_URL + "Register/Verifyemail?token=";

  useEffect(()=>{
    verifyuser(token)
  })

  const verifyuser = async(token)=>{
    try{
      let result = await axios.get(apiUrl+token);
      console.log(result)
    }catch(error){
      
    }
  }
    
    return (
    <div ><HeaderConfirm/><div style={{ textAlign: 'center', fontFamily: 'Montserrat' }}>
    <Container style={{ width: '600px', height: '720px' }}>
    <img src={require('../img/confirm-removebg-preview.png')} style={{width:'225.89px', height:'243px',marginTop: '60px' }} alt='img'/>
    <h1 style={{ fontSize: '24px', marginTop: '50px', textAlign: 'center', color: '#790B0A'  }}>Email Confirmation Success</h1>
    <p style={{textAlign: 'center'}} >Your email already! Please login first to access the web</p>
    <button onClick={(() => navigate('/'))} type="submit" style={{marginTop: '20px', backgroundColor: '#790B0A', color: 'white', padding: '10px',width: '130px', border: 'none', borderRadius: '5px', textAlign: 'center' }}>Login Here</button>
    </Container>
  </div>
</div>
    );
}
export default EmailConfirmSuccess;
