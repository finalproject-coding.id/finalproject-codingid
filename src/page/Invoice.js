import { Breadcrumbs, Button, Grid, Link, Paper, Stack, Table, TableBody, TableContainer, TableHead, TableRow } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { HeaderResponsive } from '../components/HeaderResponsive'
import { Footer } from '../components/Footer';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import { styled } from '@mui/material/styles';
import { NavLink, useNavigate } from 'react-router-dom';
import axios from 'axios';


const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#790b0a",
    color: theme.palette.common.white,
    textAlign:"center"
    
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: '#f2e7e6',
    
    
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));
export const Invoice = () => {
  const navigate = useNavigate();
  const [Invoice,setInvoice] = useState([]);
  
  const BaseAPI = process.env.REACT_APP_API_URL;
  let id = localStorage.getItem("id_users");
  const GetInvoice = () =>{
    var config = {
      method:"get",
      url:BaseAPI+"Invoice/GetInvoicebyUser?id="+id,
      headers:{}
      };
      axios(config).then((response)=>{
          console.log(response.data);
          setInvoice(response.data);
      })
  }

  useEffect(()=>{
    GetInvoice();
    if(localStorage.getItem("id_users")==null){
      console.log('kosong')
      navigate('/login');
    }
  },[])
  
  return (
    <div>
    <HeaderResponsive/>
    <Grid container sx={{mt:'2rem',minHeight:'60vh'}}>
      <Grid item xs={1}></Grid>
      <Grid item lg={10} md={10} xs={12}>
        {/* <span><a href='/landingpage' style={{}}>Home</a> &nbsp; > &nbsp;<a href='/invoice'>Invoice</a></span> */}
        <Stack spacing={2}>
          <Breadcrumbs separator=">" aria-label="breadcrumb">
            <Link underline="hover" color="Gray" href='/homepage'>
              Home
            </Link>
            <Link underline="hover" color="#790b0a" href='/invoice'>
              Invoice
            </Link>
          </Breadcrumbs>
        </Stack>
        <h3>Menu Invoice</h3>
        <Paper sx={{width:'100%'}}>
        <TableContainer >
          <Table>
          <TableHead>
            <TableRow>
              <StyledTableCell>No</StyledTableCell>
              <StyledTableCell>No. Invoice</StyledTableCell>
              <StyledTableCell>Date</StyledTableCell>
              <StyledTableCell>Total Course</StyledTableCell>
              <StyledTableCell>Total Price</StyledTableCell>
              <StyledTableCell>Action</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {Invoice.map((item,key)=>(
              <StyledTableRow key={key}>
              <TableCell sx={{textAlign:'center'}}>{key+1}</TableCell>
              <TableCell sx={{textAlign:'center'}}>{item.id_invoice}</TableCell>
              <TableCell sx={{textAlign:'center'}}>{item.tanggalinvoice}</TableCell>
              <TableCell sx={{textAlign:'center'}}>{item.totalcourse}</TableCell>
              <TableCell sx={{textAlign:'center'}}>IDR {new Intl.NumberFormat('id-ID', {currency: 'IDR' }).format(item.subtotal)}</TableCell>
              <TableCell sx={{textAlign:'center'}}>
                <NavLink component={Link} to={`/detailinvoice`}
                  state={{
                    id_invoice:item.id_invoice,
                    date: item.tanggalinvoice,
                    subtotal: item.subtotal,
                  }}
                  >
                  <Button  type='outlined' sx={{color:'#790b0a',backgroundColor:'white', border:'2px solid', borderRadius:'10px', width:'100px'}}>Details</Button>
                </NavLink>
              </TableCell>
              </StyledTableRow>
            ))}
            
          </TableBody>
          </Table>
        </TableContainer>
        </Paper>
      </Grid>
      <Grid item xs={1}></Grid>
    </Grid>
    <Footer/>
    </div>
    
     
  )
}
