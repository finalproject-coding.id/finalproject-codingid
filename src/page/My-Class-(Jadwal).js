import React, { useEffect, useState } from 'react'
import { HeaderResponsive } from '../components/HeaderResponsive'
import { Footer } from '../components/Footer'
import { Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@mui/material'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'

export const Myclassjadwal = () => {
    const navigate = useNavigate();
    const BaseAPI = process.env.REACT_APP_API_URL;
    const id_users = localStorage.getItem("id_users");
    const [Myclass, setMyclass] = useState([]);
    const GetMyClass = () =>{
        var config = {
        method:"get",
        url:BaseAPI+"MyClass/myclass?id_users="+id_users,
        headers:{}
        };
        axios(config).then((response)=>{
            console.log(response.data);
            setMyclass(response.data);
        })
    }
    useEffect(()=>{
        if(localStorage.getItem("id_users")==null){
            navigate('/login');
        }
        GetMyClass();
    },[])
    
  return (
    <div>
        <HeaderResponsive/>
        <Grid container sx={{minHeight:'60vh'}}>
            <Grid item lg={1} md={1} xs={0}></Grid>
            <Grid item lg={10} md={10} xs={12}>
                <TableContainer component={Paper} sx={{boxShadow:'none'}}>
                    <Table>
                        <TableBody>
                            {Myclass.map((item,key)=>(
                                <TableRow key={key}>
                                <TableCell align="left" width="200">
                                    <img src={item.gambar} alt="gambar produk" width="200" height="133.33" />
                                </TableCell>
                                <TableCell align="left">
                                    <Typography >
                                    {item.namatipe}
                                    </Typography>
                                    <Typography fontSize='1.5rem' fontWeight="bold" >
                                    {item.nama_kelas}
                                    </Typography>
                                    <Typography sx={{color:'#790b0a'}} fontSize='1.1rem'>
                                    Schedule : {item.tanggal_kelas}
                                    </Typography>
                                </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                
            </Grid>
            <Grid item lg={1} md={1} xs={0}></Grid>
        </Grid>
        <Footer/>
    </div>
  )
}
