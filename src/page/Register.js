import React, { useEffect, useState } from 'react'
import { Box, Button, Container, Grid, TextField } from '@mui/material';
import { Headerawal } from '../components/HeaderAwal';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import AlertModal from '../components/AlertModal';
import { validName, validPassword } from '../components/Regex';

function Register () {
  const [showModal, setShowModal] = useState(false);
  const [modalMassage, setModalMassage] = useState("");
  const [modalTitle, setModalTitle] = useState("");
  //regex Nama dan Password
  const [namaErr, setnamaErr] = useState(false);
  const [pwdError, setPwdError] = useState(false);
  //url untuk method post database
  const apiUrl = process.env.REACT_APP_API_URL + "Register/RegisterUser";
  // console.log(`${apiUrl}Register/RegisterUser`)
  const navigate = useNavigate();

  const [WindowSize, setwindowsize] = useState(window.innerWidth);
  const [brown, setbrown] = useState(0);
  const [pink, setpink] = useState(0);
  useEffect(()=>{
    const handleresize = () => {
      setwindowsize(window.innerWidth);
    };
    window.addEventListener('resize',handleresize);
    if(WindowSize < 900){
      setbrown(8);
      setpink(2);
      console.log(WindowSize);
      
    }else{
      setbrown(6);
      setpink(3);
    }
   
  },[WindowSize])

  const handleCloseModal = () => {
    setShowModal(false);
  };


  //Register
  const [data,setData] = useState({
    email:"",
    name:"",
    password:"",
    cpassword:"",
  });

  const headlerinput = (e) =>{
    const name = e.target.name;
    const value = e.target.value;

    setData({...data,[name]:value});
  }
  // console.log(data);
  const heandlerSubmit =(e)=>{
    e.preventDefault();
    const newuser = {
      nama:data.name,
      email:data.email,
      password:data.password
    };

    if(!validName.test(newuser.nama)){
      setnamaErr(true);
    }else if(validName.test(newuser.nama)){
      setnamaErr(false);
    }
    if(!validPassword.test(newuser.password)){
      setPwdError(true);
    }else if(validPassword.test(newuser.password)){
      setPwdError(false);
    }
    
    if(validName.test(newuser.nama)&&validPassword.test(newuser.password)){
      setnamaErr(false);
      setPwdError(false);
      if (data.password === data.cpassword) {
        let data = JSON.stringify(newuser);
        // console.log(newuser);
        let config = {
          method: "post",
          maxBodyLength: Infinity,
          url: apiUrl,
          headers: {
            "Content-Type": "application/json",
          },
          data: data,
        };
        axios
          .request(config)
          .then((response) => {
            if(response.data === "Success"){
              console.log(JSON.stringify(response.data));
              setShowModal(true);
              setModalTitle("Congratulation");
              setModalMassage("Activated your Email !");
            }else{
              setShowModal(true);
              setModalTitle("Error!!");
              setModalMassage( response.data+" !!");
            }
            
          })
          .catch((error) => {
            console.log(error);
          });
      } else {
        setShowModal(true);
        setModalTitle("Error!");
        setModalMassage("Password do not match!");
      }
    }
  }

  // console.log(window.innerWidth)
  return (
    <div>
      <Headerawal/>
      <Grid container>
        <Grid item xs={pink}></Grid>
        <Grid item xs={brown}>
          <Box><h1 style={{color:'#790b0a'}}>Lets Join our course!</h1></Box>
          <Box sx={{fontSize:'18px',mb:'50px'}}>Pelase register first</Box>
          <form onSubmit={heandlerSubmit}>
          <Box sx={{mt:'20px'}}> <TextField required pattern="[a-zA-z]+" sx={{width:'100%'}} id="outlined-basic" type='Text' label="Name" name='name' onChange={headlerinput} variant="outlined"/></Box>
            {namaErr && <p style={{color:'red'}}>Your Nama is Invalid</p>}
          <Box sx={{mt:'20px'}}> <TextField sx={{width:'100%'}} id="outlined-basic" type='email' label="Email" name='email' onChange={headlerinput} variant="outlined"/></Box>
          <Box sx={{mt:'20px'}}><TextField required sx={{width:'100%'}} id="outlined-basic" type='password' label="Password" name='password' onChange={headlerinput} variant="outlined"/></Box>
            {pwdError && <p style={{color:'red'}}>*Password weak, must have lowercase, uppercase and number</p>}
          <Box sx={{mt:'20px'}}><TextField required sx={{width:'100%'}} id="outlined-basic" type='password' label="Confirm Password" name='cpassword' onChange={headlerinput} variant="outlined"/></Box>
          <Box sx={{mt:'30px',textAlign:'end'}}><Button type='submit' variant='contained' sx={{backgroundColor:'#790b0a',width:'150px', borderRadius:'10px'}}>Sign Up</Button></Box>
          <Box sx={{mt:'40px',textAlign:'center'}}>Have account? <Button variant='text' onClick={(() => navigate('/login'))}>Login here</Button></Box>
          </form>
        </Grid>
        <Grid item xs={brown} ></Grid>
      </Grid>
      <AlertModal
        open={showModal}
        onClose={handleCloseModal}
        title={modalTitle}
        message={modalMassage}
      />

    </div>
  )
}
export default Register;