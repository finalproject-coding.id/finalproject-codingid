import { Box, Button, Container, Grid, Typography,Toolbar,AppBar } from '@mui/material';
import React from 'react'
import { useNavigate } from 'react-router-dom';
import { Headeruser } from '../components/Headeruser';
import HomeIcon from '@mui/icons-material/Home';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';

export const SuccessPurchase = () => {
    const navigate = useNavigate();
  return (
    <div>
        <Box sx={{ flexGrow: 1}} >
            <AppBar position="static" sx={{backgroundColor:'white',boxShadow: 'none'}}>
        <Toolbar sx={{marginLeft:'30px', marginRight:'30px'}}>
            <img src={require('../img/Untitled.png')} style={{width:'80px', height:'Auto'}} alt='img'/>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1, color:'black' }}>
            Otomobil
          </Typography>
        </Toolbar>
      </AppBar>
      </Box>
      <Grid container>
          <Grid item xs={12} sx={{textAlign:"center", mt:"40px"}} >
              <img style={{width:"300px"}} src={require("../img/confirm-removebg-preview.png")} alt="Gambar Sip"/>
              <h2 style={{color:"#790b0a"}}>Purchase Successfully</h2>
              <text style={{color:"gray", fontSize:"18px"}}>That's Great We're ready for driving day</text><br/>
              <Button onClick={(() => navigate('/homepage'))} variant='outlined' sx={{mt:'40px',color:'#790b0a',width:'180px', borderRadius:'10px', borderColor:'#790b0a','&:hover': {borderColor: '#790b0a'}}}><HomeIcon/>&nbsp; Back To Home</Button>
              <Button onClick={(() => navigate('/invoice'))} variant='contained' sx={{mt:'40px',backgroundColor:'#790b0a',width:'180px', marginLeft:'20px', borderRadius:'10px' ,'&:hover': {backgroundColor: '#790b0a'}}}><ArrowForwardIcon/>&nbsp; open invoice</Button>
          </Grid>
      </Grid>

    </div>
  )
}
