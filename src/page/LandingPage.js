import { CardActionArea, Container, Grid } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { Footer } from '../components/Footer';
import '../css/landingpage.css';
import { Headerawal } from '../components/HeaderAwal';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';



export default function LandingPage() {
  const navigate = useNavigate();
  const BaseAPI = process.env.REACT_APP_API_URL;
  const [GetTypeClass,setTypeClass] = useState([]);
  const [GetAllClass, setAllClass] = useState([]);
  const [GetClassLanding, setClassLanding] = useState([]);
  const [allUser, setAllUser] = useState();
  const apiUrl = process.env.REACT_APP_API_URL +"User/GetUser?name=";
  const token = localStorage.getItem('Token');
  const GetType = ()=>{
    var config = {
      method:"get",
      url:BaseAPI+"Class/GetTypeClass",
      headers:{}
      };
      axios(config).then((response)=>{
          // console.log(response.data);
          setTypeClass(response.data);
      })
  }
  const GetCourse = () =>{
    var config = {
      method:"get",
      url:BaseAPI+"Class/GetClassLanding",
      headers:{}
      };
      axios(config).then((response)=>{
          // console.log(response.data);
          setClassLanding(response.data);
      })
  }
  const GetCourseAll = () =>{
    var config = {
      method:"get",
      url:BaseAPI+"Class/GetClass",
      headers:{}
      };
      axios(config).then((response)=>{
          // console.log(response.data);
          setAllClass(response.data);
      })
  }

  const GetUser = ()=>{
    var config = {
        method:"get",
        url:BaseAPI+"User/GetuUerLanding",
        headers:{}
    };
    axios(config).then((response)=>{
        // console.log(response.data);
        setAllUser(response.data);
    })
}


  useEffect(()=>{
    GetType();
    GetCourse();
    GetUser();
    GetCourseAll();
  },[])

  const jumlahType = GetTypeClass.length;

  const jumlahClass = GetAllClass.length;


  return (
    <div>
      <Container style={{ maxWidth: '100%', padding: '0px', fontFamily: 'Montserrat' }}>
        <Headerawal />
        <div class='background' style={{ maxWidth: '100%', margin: '0 auto' }}>
          <h2 style={{ textAlign: 'center', color: 'white', fontSize: '32px' }}>We provide driving lessons for various types of cars</h2>
          <p style={{ textAlign: 'center', color: 'white', marginTop: '0px', fontSize: '24px' }}>Professional staff who are ready to help you to become a much-needed reliable driver</p>
          <Grid container spacing={2} style={{ marginTop: '20px' }}>
            <Grid item xs={12} sm={4}>
              <h1 style={{ backgroundColor: 'transparent', height: '50px', textAlign: 'center', color: 'white', fontSize: '48px' }}>{jumlahClass-1}+</h1>
              <p style={{ textAlign: 'center', color: 'white', fontSize: '16px' }}>A class ready to make you a reliable driver</p>
            </Grid>
            <Grid item xs={12} sm={4}>
              <h1 style={{ backgroundColor: 'transparent', height: '50px', textAlign: 'center', color: 'white', fontSize: '48px' }}>{jumlahType-1}+</h1>
              <p style={{ textAlign: 'center', color: 'white', fontSize: '16px' }}>Different car type you can choose </p>
            </Grid>
            <Grid item xs={12} sm={4}>
              <h1 style={{ backgroundColor: 'transparent', height: '50px', textAlign: 'center', color: 'white', fontSize: '48px' }}>{allUser}+</h1>
              <p style={{ textAlign: 'center', color: 'white', fontSize: '16px' }}>Active customers around the world</p>
            </Grid>
          </Grid>
          {/* <Grid container spacing={2} style={{ marginTop: '20px' }}>
            <Grid item xs={12} sm={4}>
              <h1 style={{ backgroundColor: 'transparent', height: '50px', textAlign: 'center', color: 'white', fontSize: '48px' }}>50+</h1>
              <p style={{ textAlign: 'center', color: 'white', fontSize: '16px' }}>A class ready to make you a reliable driver</p>
            </Grid>
            <Grid item xs={12} sm={4}>
              <h1 style={{ backgroundColor: 'transparent', height: '50px', textAlign: 'center', color: 'white', fontSize: '48px' }}>20+</h1>
              <p style={{ textAlign: 'center', color: 'white', fontSize: '16px' }}>Professional workforce with great experience </p>
            </Grid>
            <Grid item xs={12} sm={4}>
              <h1 style={{ backgroundColor: 'transparent', height: '50px', textAlign: 'center', color: 'white', fontSize: '48px' }}>10+</h1>
              <p style={{ textAlign: 'center', color: 'white', fontSize: '16px' }}>Cooperate with driver service partners</p>
            </Grid>
          </Grid> */}
        </div>
        <div class='listmobil' style={{ maxWidth: '100%', paddingTop: '80px' }}>
          <h1 style={{ textAlign: 'center', color: '#790b0a' }}>Join us for the course</h1>
          <Grid container spacing={2} style={{ marginTop: '20px', paddingTop: '80px', display: 'flex', justifyContent: 'center' }}>
          {GetClassLanding.map((item,key)=>(
              <Grid item xs={12} sm={4}>
              <CardActionArea
              component={Link}
              to={`/detailkelaslogin/${item.pk_id_type}`}
              state={{
                id:item.pk_id_class,
                type:item.nama_type,
                nama:item.nama_class,
                harga:item.harga_class,
                img:item.img_class,
              }}
              >
              <img style={{ width: '100%', height: 'auto' }} src={item.img_class} alt='Course SUV Kijang Innova' />
              <p style={{ textAlign: 'left', marginBottom: '0px' }}>{item.nama_type}</p>
              <p style={{ textAlign: 'left', fontWeight: 'bold', marginTop: '0px' }}>{item.nama_class}</p>
              <p style={{ textAlign: 'left', color: '#790b0a', fontWeight: 'bold', marginTop: '35px' }}>IDR {new Intl.NumberFormat('id-ID', {currency: 'IDR' }).format(item.harga_class)}</p>
              </CardActionArea>
              </Grid>
            ))}
          </Grid>
        </div>

        <div style={{ paddingTop: '165px', maxWidth: '100%' }}>
          <Grid container spacing={2} alignItems="center">
            <Grid item xs={12} md={6}>
              <h1 style={{ color: '#790b0a', fontSize: '40px', marginLeft: '80px' }}>Gets your best benefit</h1>
              <p style={{ textAlign: 'justify', fontSize: '18px', marginRight: '10px', marginLeft: '80px' }}>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam.<br></br> <br></br> Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
            </Grid>
            <Grid item xs={12} md={6}>
              <img src={require('../img/sasd-removebg-preview (1).png')} style={{ width: '100%', height: 'auto' }} />
            </Grid>
          </Grid>
        </div>

        <div>
          <h1 style={{ textAlign: 'center', paddingTop: '140px', color: '#790b0a' }}>More car type you can choose</h1>
          <Grid container spacing={2} justify="center" alignItems="center" style={{ paddingTop: '80px' }}>
             {GetTypeClass.map((item,key)=>(
              <Grid item xs={12} sm={6} md={3}>
              <img src={item.img_type} alt='Course SUV Kijang Innova' onClick={()=>(navigate('/listmenukelaslogin/'+item.pk_id_type))} style={{ display: 'block', margin: '0 auto' }}/>
              <p style={{ textAlign: 'center' }}>{item.nama_type}</p>
              </Grid>
             ))}
          </Grid>
        </div>
        <div style={{ paddingTop: '180px' }}>
          <Footer />
        </div>
      </Container>
    </div>
  );
}
