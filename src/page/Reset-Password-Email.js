import React, { useEffect, useState } from 'react'
import { Headerawal } from '../components/HeaderAwal'
import { Button, Grid, TextField, Box } from '@mui/material'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'
import AlertModal from '../components/AlertModal'

const ResetPasswordEmail = () => {
  const navigate = useNavigate();
  const [email,setEmail] = useState("");
  const apiUrl = process.env.REACT_APP_API_URL + "User/ForgotPassword?email=";
  const [WindowSize, setwindowsize] = useState(window.innerWidth);
  const [brown, setbrown] = useState(0);
  const [pink, setpink] = useState(0);
  const [showModal, setShowModal] = useState(false);
  const [modalMassage, setModalMassage] = useState("");
  const [modalTitle, setModalTitle] = useState("");
  useEffect(() => {
    const handleresize = () => {
      setwindowsize(window.innerWidth);
    };
    window.addEventListener('resize', handleresize);
    if (WindowSize < 900) {
      setbrown(8);
      setpink(2);
      console.log(WindowSize);

    } else {
      setbrown(6);
      setpink(3);
    }

  }, [WindowSize])


  const handlereset = (e) =>{
    e.preventDefault()
    if(email === "" || email === null){
      setShowModal(true);
      setModalTitle("Error!");
      setModalMassage("Email Can't Empty");
    }
    resetpassword(email);
    
  }

  const resetpassword = async(email)=>{
    try{
      let result = await axios.get(apiUrl+email);
      if(result.data === "User not found"){
        setShowModal(true);
        setModalTitle("Error!");
        setModalMassage("User Not Found");
      }
      else{
        setShowModal(true);
        setModalTitle("Success!");
        setModalMassage("Check Your Email");
      }
    }catch(error){
      
    }
  }
  const handleCloseModal = () => {
    setShowModal(false);
  };
  return (
    <div>
      <Headerawal />
      <Grid container>
        <Grid item xs={pink}></Grid>
        <Grid item xs={brown}>
          <Box sx={{ fontSize: '24px', marginTop: '80px' }}>Reset Password</Box>
          <Box sx={{ fontSize: '18px', mt: '10px', color: 'gray' }}>Send OTP code to your email address</Box>
          <form onSubmit={handlereset}>
          <Box sx={{ mt: '40px' }}> <TextField sx={{ width: '100%' }} id="outlined-basic" type='email' label="Email" onChange={(e)=>setEmail(e.target.value)} variant="outlined" /></Box>
          <Box sx={{ mt: '30px', display: 'flex', justifyContent: 'flex-end' }}>
            <Button onClick={(() => navigate('/login'))} variant='contained' sx={{ backgroundColor: '#fff', color: '#790b0a', width: '150px', borderRadius: '10px', border: '2px solid #790b0a', '&:hover': { backgroundColor: '#fff' }, mr: 2 }}>Cancel</Button>
            <Button type='submit' variant='contained' sx={{ backgroundColor: '#790b0a', width: '150px', borderRadius: '10px', '&:hover': { backgroundColor: '#790b0a' }, ml: 2 }}>Confirm</Button>
          </Box>
          </form>
        </Grid>
        <Grid item xs={pink} ></Grid>
      </Grid>
      <AlertModal
        open={showModal}
        onClose={handleCloseModal}
        title={modalTitle}
        message={modalMassage}
      />
    </div>
  )
}

export default ResetPasswordEmail;