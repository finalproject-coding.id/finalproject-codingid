import { Card, CardActionArea, CardContent, CardMedia, Grid } from '@mui/material';
import React, { useEffect, useState } from 'react';
import "../../css/landingpage.css";
import { Link, useNavigate, useParams } from 'react-router-dom';
import axios from 'axios';
import { Headerawal } from '../../components/HeaderAwal';
import { Footer } from '../../components/Footer';


export const ListMenuKelasLogin = () => {
  const navigate = useNavigate();
  let {id} = useParams();
  const BaseAPI = process.env.REACT_APP_API_URL;
  const [CoursebyID, Setcourse] = useState([]);

  const [GetTypeClass,setTypeClass] = useState([]);

  const GetType = ()=>{
    var config = {
      method:"get",
      url:BaseAPI+"Class/GetTypeClassid?id="+id,
      headers:{}
      };
      axios(config).then((response)=>{
          // console.log(response.data);
          setTypeClass(response.data);
      })
  }
 
  const GetCourse = () =>{
    var config = {
      method:"get",
      url:BaseAPI+"Class/GetClassid?id="+id,
      headers:{}
      };
      axios(config).then((response)=>{
          // console.log(response.data);
          Setcourse(response.data);
      })
  }
  useEffect(()=>{
    GetCourse();
    GetType();
    
  },[]);

console.log(CoursebyID);
  

  return (
    <div>
      <Headerawal/>
      
      <Grid container sx={{minHeight:'60vh'}}>
        <Grid item className='backgroundlistkelas' >
           
        </Grid>
        <Grid item ml={10} mr={10}>
          <h1>{GetTypeClass}</h1>
          <span style={{fontSize:'20px', textAlign:'justify',color:'gray'}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
          voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
          mollit anim id est laborum.</span>
        </Grid>
        <Grid xl={12} item ml={5} mr={5} sx={{marginTop:'80px', width:'100%'}}>
            <hr></hr>
            <h1 style={{color:'#790b0a',textAlign:'center', marginBottom:'80px'}}>Another favorite course</h1>
            <Grid container spacing={2}>
              {CoursebyID.map((item,key)=>(
                <Grid item xl={4}>
                <Card sx={{ maxWidth:'345px', height:'450px', marginBottom:'20px', textDecoration:"none"}} >
                  <CardActionArea component={Link}
                  to={`/detailkelaslogin/${id}`}
                  state={{
                    id:item.pk_id_class,
                    type:item.nama_type,
                    nama:item.nama_class,
                    harga:item.harga_class,
                    img:item.img_class,
                  }}>
                  {/* <CardMedia sx={{ maxWidth:'345px', height:'450px', marginBottom:'20px', textDecoration:"none"}} 
                  component={Link}
                  to={{
                    pathname:"/detailkelas/1",
                    state:{
                      id:item.pk_id_class,
                      nama:item.nama_class,
                    }
                  }}
                  > */}
                  <CardMedia
                    component="img"
                    alt="green iguana"
                    height="auto"
                    image={item.img_class}
                  />
                  <CardContent>
                    <h3 style={{ color:'gray'}}>{item.nama_type}</h3>
                    <h3>{item.nama_class}</h3>
                    <h2 style={{color:'#790b0a',marginTop:'40px'}}>
                      IDR {new Intl.NumberFormat('id-ID', {currency: 'IDR' }).format(item.harga_class)}
                    </h2> 
                  </CardContent>
                  </CardActionArea>
                </Card> 
                </Grid>
              ))}
            </Grid>
        </Grid>
      </Grid>
      <Footer/>
    </div>
  )
}
