import { Box, Button ,Card, CardContent, CardMedia, Grid, FormControl , InputLabel,Select,MenuItem, CardActionArea   } from '@mui/material';
import React, { useEffect, useState } from 'react';
import "../../css/landingpage.css";
import { Link, useLocation, useNavigate, useParams } from 'react-router-dom';
import axios from 'axios';
import { Headerawal } from '../../components/HeaderAwal';
import { Footer } from '../../components/Footer';
import AlertModal from '../../components/AlertModal';

export const DetailKelasLogin = ({route}) => {
    const navigate = useNavigate();
    const [showModal, setShowModal] = useState(false);
    const [modalMassage, setModalMassage] = useState("");
    const [modalTitle, setModalTitle] = useState("");
    let hasil = {
      img:"",
      type:"",
      nama:"",
      harga:0,
    }
    let data = useLocation();
   
    // console.log(data);
    if(data.state != null){
      hasil = {
        img:data.state.img,
        type: data.state.type,
        nama: data.state.nama,
        harga:data.state.harga,
      }
    }else{
      navigate('/landingpage');
    }
    console.log(hasil);
    let {id} = useParams();
    const BaseAPI = process.env.REACT_APP_API_URL;
    const [CoursebyID, Setcourse] = useState([]);
    const GetCourse = () =>{
      var config = {
        method:"get",
        url:BaseAPI+"Class/GetClassid?id="+id,
        headers:{}
        };
        axios(config).then((response)=>{
            Setcourse(response.data);
        })
    }
    useEffect(()=>{
      GetCourse();
    },[])
    let d1= new Date();
    d1.setDate(d1.getDate() + 1);
    let d2= new Date();
    d2.setDate(d2.getDate() + 2);
    let d3= new Date();
    d3.setDate(d3.getDate() + 3);
    let d4= new Date();
    d4.setDate(d4.getDate() + 4);
    let d5= new Date();
    d5.setDate(d5.getDate() + 5);
    let d6= new Date();
    d6.setDate(d6.getDate() + 6);
    let d7= new Date();
    d7.setDate(d7.getDate() + 7);
    var date1 = d1.toUTCString().substring(0,17);
    var date2 = d2.toUTCString().substring(0,17);
    var date3 = d3.toUTCString().substring(0,17);
    var date4 = d4.toUTCString().substring(0,17);
    var date5 = d5.toUTCString().substring(0,17);
    var date6 = d6.toUTCString().substring(0,17);
    var date7 = d7.toUTCString().substring(0,17);
    const date = [
      {"tanggal":date1},
      {"tanggal":date2},
      {"tanggal":date3},
      {"tanggal":date4},
      {"tanggal":date5},
      {"tanggal":date6},
      {"tanggal":date7}
    ]

    //  console.log(date1);
    const handleonclickaddcart = () => {
      navigate('/login');
    }
    
    const [gettanggal, settanggal] = useState();
    const handleCloseModal = () => {
      setShowModal(false);
    };

  return (
    <div>
      <Headerawal/>
      <Grid container sx={{minHeight:'60vh'}}>
        
          <Grid container ml={10} mr={10} xl={12} sx={{marginTop:'80px'}}>
          <Grid item xl={4}>
            <img src={hasil.img} style={{width:'90%'}} />
          </Grid>
          <Grid item xl={8}>
            <span style={{color:'gray', fontSize:'24px',marginBottom:'20px'}}>{hasil.type}</span><br/>
            <span style={{fontSize:'30px',fontWeight:'bold',marginBottom:'20px'}}>{hasil.nama}</span><br/>
            <span style={{color:'#790b0a', fontSize:'30px',fontWeight:'bold',marginBottom:'20px'}}>
            IDR {new Intl.NumberFormat('id-ID', {currency: 'IDR' }).format(hasil.harga)}
            </span><br/>
            <Box>
            <FormControl sx={{width:'350px', marginTop:'60px', marginBottom:'60px'}}>
              <InputLabel id="demo-simple-select-label">Select Schedule</InputLabel>
              <Select
                onChange={(e)=> settanggal(e.target.value)}
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                label="Select Schedule">
                  {date.map((value,key)=>(
                     <MenuItem value={value.tanggal}>{value.tanggal}</MenuItem>
                  ))}
              </Select>
              </FormControl>
            </Box>
            <Button variant='outlined' onClick={(handleonclickaddcart)} sx={{color:'#790b0a'}}>Add to cart</Button>
            <Button variant='contained' onClick={(handleonclickaddcart)} sx={{backgroundColor:'#790b0a', marginLeft:'20px'}}>Buy Now</Button>
          </Grid>
          </Grid>
        
        <Grid item ml={10} mr={10}>
          <h1>Description</h1>
          <span style={{fontSize:'20px', textAlign:'justify',color:'gray'}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
          voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
          mollit anim id est laborum.</span> <br/> <br/>
          <span style={{fontSize:'20px', textAlign:'justify',color:'gray' }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
          voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
          mollit anim id est laborum.</span>
        </Grid>
        <Grid xl={12} item ml={10} mr={10} sx={{marginTop:'80px', width:'100%'}}>
            <hr></hr>
            <h1 style={{color:'#790b0a',textAlign:'center', marginBottom:'80px'}}>Another favorite course</h1>
            <Grid container spacing={2}>
                
                {CoursebyID.map((item,key)=>(
                <Grid item xl={4}>
                <Card sx={{ maxWidth:'345px', height:'450px', marginBottom:'20px', textDecoration:"none"}} >
                  <CardActionArea component={Link}
                  to={`/detailkelaslogin/${id}`}
                  state={{
                    id:item.pk_id_class,
                    type:item.nama_type,
                    nama:item.nama_class,
                    harga:item.harga_class,
                    img:item.img_class,
                  }}>
                  <CardMedia
                    component="img"
                    alt="green iguana"
                    height="auto"
                    image={item.img_class}
                  />
                  <CardContent>
                    <h3 style={{ color:'gray'}}>{item.nama_type}</h3>
                    <h3>{item.nama_class}</h3>
                    <h2 style={{color:'#790b0a',marginTop:'40px'}}>
                      IDR {new Intl.NumberFormat('id-ID', {currency: 'IDR' }).format(item.harga_class)}
                    </h2> 
                  </CardContent>
                  </CardActionArea>
                </Card> 
                </Grid>
              ))}
                
            </Grid>
            
        </Grid>
      </Grid>
      <AlertModal
        open={showModal}
        onClose={handleCloseModal}
        title={modalTitle}
        message={modalMassage}
      />
      <Footer/>
    </div>
  )
}
