import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { HeaderAdmin } from '../../components/HeaderAdmin';


function Coba() {
    const navigate = useNavigate()
  const [imageSrc, setImageSrc] = useState(localStorage.getItem('gambar'));
  const [img, setImg] = useState([]);
 
  console.log(localStorage.getItem('gambar'));

  
    

  function handleImageChange(event) {
    const file = event.target.files[0];
    
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setImageSrc(reader.result);
        localStorage.setItem('gambar',reader.result);
    };

    
  }

  return (
    <div>
        <HeaderAdmin/>
      <h1>My React App</h1>
      <img src={imageSrc} alt="example" style={{maxWidth:'100px'}}/>
      <input type="file" onChange={handleImageChange} />
      
    </div>
  );
}

export default Coba;