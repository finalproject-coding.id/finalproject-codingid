import { Box, Card, CardActionArea, CardContent, CardMedia, Grid, Skeleton } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { Footer } from '../components/Footer';
import "../css/landingpage.css";
import innove from "../img/Rectangle 12-6.png";
import { HeaderResponsive } from '../components/HeaderResponsive';
import { Link, useNavigate, useParams } from 'react-router-dom';
import axios from 'axios';
import ClassCard from '../components/ClassCard';


export const ListMenuKelas = () => {
  const navigate = useNavigate();
  if(localStorage.getItem("id_users")==null){
    navigate('/login');
  }
  let {id} = useParams();
  const BaseAPI = process.env.REACT_APP_API_URL;
  const [CoursebyID, Setcourse] = useState([]);
  const [ambilcourse, setAmbilCourse] = useState({
    id:0,
    nama_type:"",
    nama_class:"",
    harga_class:"",
    img_class:""
  });
  const [GetTypeClass,setTypeClass] = useState([]);

  const GetType = ()=>{
    var config = {
      method:"get",
      url:BaseAPI+"Class/GetTypeClassid?id="+id,
      headers:{}
      };
      axios(config).then((response)=>{
          // console.log(response.data);
          setTypeClass(response.data);
      })
  }
 
  const GetCourse = () =>{
    var config = {
      method:"get",
      url:BaseAPI+"Class/GetClassid?id="+id,
      headers:{}
      };
      axios(config).then((response)=>{
          // console.log(response.data);
          Setcourse(response.data);
      })
  }

 

  useEffect(()=>{
    GetCourse();
    GetType();
    
  },[]);

  const handleDetailClick = (item)=>{
    navigate('/detailkelas/1',{
      itemid:1,
      otherparams:"ini dicoba",
    });
  }

console.log(CoursebyID);
  

  return (
    <div>
      <HeaderResponsive/>
      
      <Grid container sx={{minHeight:'60vh'}}>
        <Grid item className='backgroundlistkelas' >
           
        </Grid>
        <Grid item ml={10} mr={10}>
          <h1>{GetTypeClass}</h1>
          <span style={{fontSize:'20px', textAlign:'justify',color:'gray'}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
          voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
          mollit anim id est laborum.</span>
        </Grid>
        <Grid xl={12} item ml={10} mr={10} sx={{marginTop:'80px', width:'100%'}}>
            <hr></hr>
            <h1 style={{color:'#790b0a',textAlign:'center', marginBottom:'80px'}}>Another favorite course</h1>
            <Grid container spacing={2}>
              {CoursebyID.map((item,key)=>{
                if(CoursebyID.length!==0){
                  return(
                  <Grid item xl={4} >
                  <ClassCard
                  idtype={id} 
                  idclass={item.pk_id_class} 
                  namatype={item.nama_type} 
                  namaclass={item.nama_class} 
                  hargaclass={item.harga_class} 
                  imgclass={item.img_class}/>
                </Grid>
                  )
                }else{
                  <Skeleton variant="rectangular" width={210} height={118} />
                }
              })}
            </Grid>
        </Grid>
      </Grid>
      <Footer/>
    </div>
  )
}
