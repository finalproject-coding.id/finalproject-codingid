import React, { useState } from 'react'
import { Headerawal } from '../components/HeaderAwal'
import { Box, Button, Grid, TextField } from '@mui/material'
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import axios from 'axios';
import AlertModal from '../components/AlertModal';

const Login = () => {
  const navigate = useNavigate();
  // const history = createBrowserHistory();
  const apiUrl = process.env.REACT_APP_API_URL + "Register/Login";
  const [showModal, setShowModal] = useState(false);
  const [modalMassage, setModalMassage] = useState("");
  const [modalTitle, setModalTitle] = useState("");
  const [WindowSize, setwindowsize] = useState(window.innerWidth);
  const [brown, setbrown] = useState(0);
  const [pink, setpink] = useState(0);
  useEffect(()=>{
    const handleresize = () => {
      setwindowsize(window.innerWidth);
    };
    window.addEventListener('resize',handleresize);
    if(WindowSize < 900){
      setbrown(8);
      setpink(2);
      console.log(WindowSize);
      
    }else{
      setbrown(6);
      setpink(3);
    }
   
  },[WindowSize])

  //set data 
  const [data, setData] = useState({
    email : "",
    password:"",
  });

  const handleinput = (e) =>{
    const name = e.target.name;
    const value = e.target.value;

    setData({...data,[name]:value});
  }

  const handlelogin=(e)=>{
    e.preventDefault()
    const invalid = Object.values(data).every((x)=>x === null || x === "");
    if(invalid){
      setShowModal(true);
      setModalTitle("Error!");
      setModalMassage("Email or Password is Empty!");
    }else{
      let datauser = JSON.stringify(data);
      console.log(datauser);
      let config = {
        method: "post",
        maxBodyLength: Infinity,
        url: apiUrl,
        headers:{
          "Content-Type": "application/json",
        },
        data:datauser,
      };
      axios.request(config).then((response)=>{
        // console.log(response);
        if(response.data === "Wrong Credential"){
          setShowModal(true);
          setModalTitle("Error!");
          setModalMassage("Check your Password!");
        }else if(response.data === "User not found or Activate your email"){
          setShowModal(true);
          setModalTitle("Error!");
          setModalMassage("User not Found or Activate your email!");
        }else if(response.data === "Users Not Active"){
          setShowModal(true);
          setModalTitle("Error!");
          setModalMassage(response.data +"!!!");
        }else{
          localStorage.setItem("email",response.data.email);
          localStorage.setItem("role",response.data.role);
          localStorage.setItem("Token",response.data.token);
          localStorage.setItem("id_users",response.data.id);
          if(localStorage.getItem("role")==="users"){
            navigate('/homepage');
          }else if(localStorage.getItem("role")==="admin"){
            navigate('/homepage');
          }
        }
      })
    }
  }
  const handleCloseModal = () => {
    setShowModal(false);
  };


  // console.log(window.innerWidth)
  return (
    <div>
      <Headerawal/>
      <Grid container>
        <Grid item xs={pink}></Grid>
        <Grid item xs={brown}>
          <Box><h1 style={{color:'#790b0a'}}>Welcome Back!</h1></Box>
          <Box sx={{fontSize:'18px'}}>Pelase Login First</Box>
          <form onSubmit={handlelogin}>
            <Box sx={{mt:'40px'}}> <TextField sx={{width:'100%'}} id="outlined-basic" name='email' onChange={handleinput} type='email' label="Email" variant="outlined"/></Box>
            <Box sx={{mt:'20px'}}><TextField sx={{width:'100%'}} id="outlined-basic" name='password' onChange={handleinput} type='password' label="Password" variant="outlined"/></Box>
            <Box sx={{mt:'20px'}}>Forgot Password? <Button onClick={(() => navigate('/resetpasswordemail'))} variant='text'>Click Here</Button></Box>
            <Box sx={{mt:'30px',textAlign:'end'}}><Button type='submit' variant='contained' sx={{backgroundColor:'#790b0a',width:'150px', borderRadius:'10px'}}>Login</Button></Box>
          </form>
          <Box sx={{mt:'40px',textAlign:'center'}}>Dont have account? <Button variant='text' onClick={(() => navigate('/register'))}>sign up here</Button></Box>
        </Grid>
        <Grid item xs={brown} ></Grid>
      </Grid>
      <AlertModal
        open={showModal}
        onClose={handleCloseModal}
        title={modalTitle}
        message={modalMassage}
      />

    </div>
  )
}
export default Login;
