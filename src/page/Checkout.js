import React, { useEffect, useState } from 'react';
import { Box, Button, Container, Grid, Typography, TableRow, Table, TableHead, TableBody, TableCell, Checkbox, IconButton, Dialog, DialogTitle, DialogContent, DialogActions, RadioGroup, FormControlLabel, Radio } from '@mui/material';
import { useNavigate } from 'react-router-dom'
import { HeaderResponsive } from '../components/HeaderResponsive';
import { Delete, Key } from '@mui/icons-material';
import axios from 'axios';
import '../css/checkout.css'

export const Checkout = () => {
  const navigate = useNavigate();
  if (localStorage.getItem("id_users") == null) {
    navigate('/login');
  }
  const [isChecked, setIsChecked] = useState([]);
  const [showPaymentMethod, setShowPaymentMethod] = useState(false);
  const [selectedPaymentMethod, setSelectedPaymentMethod] = useState('');
  const [paymentMethodList, setPaymentMethodList] = useState([]);
  const [cartlist, setCartlist] = useState([]);
  const [GetClasslist, setclasslist] = useState([]);
  const [clickedIndex, setClickedIndex] = useState(null);
  const [checkedCount, setCheckedCount] = useState(0);


  const [cartview, setcartview] = useState([{
    namatipe: "",
    namakelas: "",
    tanggal: "",
    gambar: "",
    hargakelas: ""
  }]);

  const totalPrice = 0;

  const handleButtonClick = (index) => {
    setClickedIndex(index);
  };

  const handleCheckAll = (event, item) => {
    const { checked } = event.target;
    const updatedChecked = checked ? [...isChecked, item.id_cart] : isChecked.filter(id => id !== item.id_cart);
    setIsChecked(updatedChecked);
    setCheckedCount(updatedChecked.length);
    if (event.target.checked) {
      const newIsChecked = cartlist.map((item) => item.pk_cart_id);
      setIsChecked(newIsChecked);
    } else {
      setIsChecked([]);
    }
  }

  const handleCheck = (event, item) => {
    const { checked } = event.target;
    const updatedChecked = checked ? [...isChecked, item.id_cart] : isChecked.filter(id => id !== item.id_cart);
    setIsChecked(updatedChecked);
    setCheckedCount(updatedChecked.length);
    if (event.target.checked) {
      setIsChecked([...isChecked, item.id_cart]);

    } else {
      const newIsChecked = isChecked.filter((id) => id !== item.id_cart);
      setIsChecked(newIsChecked);
    }

  }
  

  const handlePayNow = () => {
    setShowPaymentMethod(true);
  }



  const handlePaymentMethodClose = () => {
    setShowPaymentMethod(false);
  }

  const handlePaymentMethodSelect = (event) => {
    setSelectedPaymentMethod(event.target.value);
  }

  const baseUrl = process.env.REACT_APP_API_URL;

  const paymentMethods = [];



  const GetPayment = () => {
    var config = {
      method: "get",
      url: baseUrl + "PaymentMethod/getpaymentcheckout",
      headers: {}
    };
    axios(config).then((response) => {
      // console.log(response.data);
      setPaymentMethodList(response.data);

    })

  }

  const GetClass = () => {
    var config = {
      method: "get",
      url: baseUrl + "Class/GetClass",
      headers: {}
    };
    axios(config).then((response) => {
      // console.log(response.data);
      setclasslist(response.data);

    })

  }

  const cart = [];

  const GetCart = () => {
    var config = {
      method: "get",
      url: baseUrl + "Cart/getcart?id_users=" + localStorage.getItem("id_users"),
      headers: {}
    };
    axios(config).then((response) => {
      setCartlist(response.data);
      GetClass();
      cekcart();

    })

  }
  let total = 0;

  const cekcart = () => {
    cartlist.map((item, key) => {
      GetClasslist.map((list, key) => {
        if (item.id_course == list.pk_id_class) {
          const tempdata = {id_course:item.id_course, id_cart: item.pk_cart_id, namatipe: list.nama_type, namakelas: list.nama_class, tanggal: item.tanggal, gambar: list.img_class, hargakelas: list.harga_class }
          total += list.harga_class;
          cart.push(tempdata);
        }
      })
    })

  }
  useEffect(() => {
    GetPayment();
    GetCart();


  }, []);
  cekcart();


  const handledeletecart = (id) => {
    // console.log(id);
    var config = {
      method: "delete",
      url: baseUrl + "Cart/deletecart?id_cart=" + id,
      headers: {}
    };
    axios(config).then((response) => {
      // console.log(response.data);
      GetCart();
    })

  }


  const handleclickpostinvoice = () => {
    const today = new Date();
    const options = { day: 'numeric', month: 'long', year: 'numeric' };
    const formattedDate = today.toLocaleDateString('id-ID', options);
    let subtotal = 0;
    let totalcourse = 0;
    const thisdate = formattedDate;
    const selectedItems = cart
      .filter((item) => isChecked.includes(item.id_cart))
      .map((item) => ({id_course:item.id_course, id_cart: item.id_cart, namakelas: item.namakelas, namatipe: item.namatipe, tanggal: item.tanggal, hargakelas: item.hargakelas, gambar: item.gambar }));

    selectedItems.map((item) => {
      subtotal += item.hargakelas;
      totalcourse++;
    }
    
    )
    
    // console.log(totalcourse);

    // let data = JSON.stringify(selectedItems);
    // console.log(data);
    const result = {
      id_users: localStorage.getItem("id_users"),
      subtotal: subtotal,
      totalcourse: totalcourse,
      tanggalbeli: thisdate,
      dinvoice: selectedItems
    }
    let invoice = JSON.stringify(result);
    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: baseUrl + "Invoice/addinvoice",
      headers: {
        "Content-Type": "application/json",
      },
      data: invoice,
    };
    axios.request(config).then((response) => {
      // console.log(response.data);
      if (response.data === "Success") {
        navigate('/successpurchase');
      }
    })
  }



  return (
    <Container style={{ maxWidth: '100%', padding: '0px', fontFamily: 'Montserrat' }}>
      <HeaderResponsive />
      <div align='center' style={{ minHeight: '71vh', padding: "0px" }}>
        <Grid >
          <Grid style={{ overflowX: 'auto' }}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell align="left">
                    <Checkbox
                      alt="checkall"
                      checked={isChecked.length === cartlist.length}
                      onChange={handleCheckAll}
                      color="primary"
                      inputProps={{ 'aria-label': 'select all items' }}
                    />
                  </TableCell>
                  <TableCell align="left">
                    <Typography variant="h6"  >
                      Pilih Semua
                    </Typography>
                  </TableCell>
                  <TableCell align="left">
                    <Typography variant="h6" >
                    </Typography>
                  </TableCell>
                  <TableCell align="right">
                    <Typography variant="h6" >
                    </Typography>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>

                {cart.map((item, key) => (
                  <TableRow key={key}>
                    <TableCell>
                      <Checkbox
                        alt="checkindividu"
                        checked={isChecked.includes(item.id_cart)}
                        onChange={(event) => handleCheck(event, item)}
                        color="primary"
                        inputProps={{ 'aria-label': `select ${item.namakelas}` }}
                      />
                    </TableCell>
                    <TableCell align="left" width="200" height="133.33">
                      <img src={item.gambar} alt="gambar produk" width="200" height="133.33" />
                    </TableCell>
                    <TableCell align="left">
                      <Typography variant="body1">
                        {item.namatipe}
                      </Typography>
                      <Typography variant="body2">
                        {item.namakelas}
                      </Typography>
                      <Typography variant="body2">
                        Schedule: {item.tanggal}
                      </Typography>
                      <Typography variant="body2">
                        IDR {new Intl.NumberFormat('id-ID', { currency: 'IDR' }).format(item.hargakelas)}
                      </Typography>
                    </TableCell>
                    <TableCell align="right">
                      <IconButton aria-label="delete" color="error" onClick={() => handledeletecart(item.id_cart)}>
                        <Delete />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Grid>


        </Grid>
      </div>
      <Box sx={{ display: 'flex', justifyContent: 'space-between', marginTop: '20px', padding: '40px', boxShadow: 3 }}>
        <Typography variant="h6">
          Total Price IDR{' '}
          {new Intl.NumberFormat('id-ID', { currency: 'IDR' }).format(
            cart.reduce((total, item) => {
              if (isChecked.includes(item.id_cart)) {
                return total + item.hargakelas;
              }
              return total;
            }, 0)
          )}
        </Typography>

        <Button variant="contained" size="large" sx={{
          color: '#790b0a', backgroundColor: '#790b0a', color: 'white', textTransform: 'none', '&:hover': {
            backgroundColor: '#790b0a',
          }
        }} disabled={checkedCount === 0} onClick={handlePayNow}>
          Pay Now
        </Button>
      </Box>
      <Dialog open={showPaymentMethod} onClose={handlePaymentMethodClose}>
        <DialogTitle style={{ fontFamily: 'Montserrat' }}>Select Payment Method</DialogTitle>
        <DialogContent>
          <RadioGroup
            aria-label="paymentMethod"
            name="paymentMethod"
            value={selectedPaymentMethod}
            onChange={handlePaymentMethodSelect}
          >
            {paymentMethodList.map((data, index) => (
              <Button
                key={index}
                variant="outlined"
                color="primary"
                style={{
                  fontFamily: 'Montserrat',
                  border: `1px solid ${clickedIndex === index ? '#790b0a' : 'transparent'}`,
                  background: 'transparent',
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.5rem',
                  justifyContent: 'flex-start',
                }}
                onClick={() => handleButtonClick(index)}
              >
                <img src={data.logoUrl} style={{ width: '24px', height: '24px' }} alt={data.name} />
                <span style={{ color: 'black', fontWeight: '500' }}>{data.name}</span>
              </Button>
            ))}
          </RadioGroup>
        </DialogContent>
        <DialogActions sx={{ display: 'flex', justifyContent: 'space-between' }}>
          <Button sx={{ color: '#790b0a', backgroundColor: 'white', textTransform: 'none' }} onClick={handlePaymentMethodClose} color="secondary">
            Cancel
          </Button>
          <Button sx={{
            color: 'white',
            backgroundColor: '#790b0a',
            textTransform: 'none',
            '&:hover': {
              backgroundColor: '#790b0a',
            },
          }} disabled={clickedIndex === null} onClick={handleclickpostinvoice} color="primary">
            Pay Now
          </Button>
        </DialogActions>
      </Dialog>
    </Container>
  )
}
